package orsc.musicplayer;



import java.io.ByteArrayInputStream;
import java.io.File;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Soundbank;
import javax.sound.midi.Synthesizer;
import javax.sound.midi.Track;

import orsc.Config;

final class Class56_Sub1_Sub1 extends Class56_Sub1 implements Receiver {

   private static Receiver receiver = null;
   private static Sequencer sequencer = null;
   private static Synthesizer synth = null;
   private static File soundsetFile = null;
   
   private static boolean modMidis = false;//use for rev. #600+ pre-RS3 midis
   
   private static String soundfontFile = Config.F_CACHE_DIR+"/soundfont/8bitsf.sf2";

   final void method827(int i, byte[] data, int i_0_, boolean bool) {
      if(sequencer != null) {
         try {
            Sequence sequence = MidiSystem.getSequence(new ByteArrayInputStream(data));
            if(modMidis)
            	modMidi(sequence);
            sequencer.setSequence(sequence);
            sequencer.setLoopCount(!bool?0:-1);
            this.method835(0, i, -1L);
            sequencer.start();
         } catch (Exception var6) {
            ;
         }
      }

   }
   
   private void modMidi(Sequence sequence) throws InvalidMidiDataException{
	   int BankMSB = 0;
	   int BankLSB = 0;
	   for (Track track : sequence.getTracks()) {
		   for (int i = 0; i < track.size(); i++) {
			   MidiEvent event = track.get(i);
			   MidiMessage message = event.getMessage();
			   if (message instanceof ShortMessage) {
				   ShortMessage shortMessage = (ShortMessage) message;
				   if (shortMessage.getCommand() == ShortMessage.CONTROL_CHANGE) {
					   int channel = shortMessage.getChannel();
					   int status = shortMessage.getStatus();
					   int bytes = shortMessage.getData1();
					   int value = shortMessage.getData2();
					   if (bytes == 32) {
						   BankLSB = value;
						   shortMessage.setMessage(status, channel, 0, value);
					   }
					   if (bytes == 0) {
						   shortMessage.setMessage(status, channel, 32, BankLSB);
					   }
				   }
			   }
		   }
	   }
   }

   final void method833() {
      if(sequencer != null) {
         sequencer.stop();
         this.method838(-1L);
      }

   }

   public final synchronized void send(MidiMessage midimessage, long l) {
      byte[] is = midimessage.getMessage();
      if(is.length < 3 || !this.method837(is[0], is[1], is[2], l)) {
         receiver.send(midimessage, l);
      }

   }

   Class56_Sub1_Sub1() {
      try {
    	  Soundbank soundbank = null;//custom
    	  File f = new File(soundfontFile);
    	  if(soundsetFile == null && f.exists()){
    		  soundsetFile = f;
    	  }
    	  if(soundsetFile != null){//custom
    		  soundbank = MidiSystem.getSoundbank(soundsetFile);
    	  }
    	 
         receiver = MidiSystem.getReceiver();
         sequencer = MidiSystem.getSequencer(false);
         sequencer.getTransmitter().setReceiver(this);
         
         if(MusicPlayer.loadCustomSoundfont){//custom
        	 synth = MidiSystem.getSynthesizer();
         }
         
         sequencer.open();
         
         if(MusicPlayer.loadCustomSoundfont && soundbank != null){//custom
        	 synth.open();
        	 synth.loadAllInstruments(soundbank);
        	 sequencer.getTransmitter().setReceiver(synth.getReceiver());
        	 receiver = synth.getReceiver();
         }
         
         
         this.method838(-1L);
      } catch (Exception var2) {
         MusicPlayer.method790();
      }

   }

   final void method828() {
      if(sequencer != null) {
         sequencer.close();
         sequencer = null;
      }

      if(receiver != null) {
         receiver.close();
         receiver = null;
      }

   }

   public final void close() {}

   final void method831(int i) {
      if(sequencer != null) {
         this.method840(i, -1L);
      }

   }

   final synchronized void method830(int i, int i_2_) {
      if(sequencer != null) {
         this.method835(i_2_, i, -1L);
      }

   }

   final void method836(int i, int i_5_, int i_6_, long l) {
      try {
         ShortMessage invalidmididataexception = new ShortMessage();
         invalidmididataexception.setMessage(i, i_5_, i_6_);
         receiver.send(invalidmididataexception, l);
      } catch (InvalidMidiDataException var7) {
         ;
      }

   }

   final void method832(int i) {
      if(i > -90) {
         this.method833();
      }

   }

}
