package orsc.musicplayer;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import orsc.Config;
import orsc.mudclient;
import orsc.util.FileOperations;

public class MusicPlayer {
	
	
	/*
	 TODO:
	 - FIX: volume control does not work when loading custom soundfont
	 - change how fading out is done, current way of doing it often has some volume issues.. 
	 - refactor a lot (as its pretty much just ripped from rs2 client)
	  
	 */

	public static int anInt1401 = 256;
	public static int[] anIntArray385 = new int[]{12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800, 12800};
	public static boolean loadCustomSoundfont = true;
	public static Class56 aClass56_749;
	public static int anInt720 = 0;
	public static int anInt478 = -1;
	public static byte[] aByteArray347;
	public static int anInt155 = 0;
	public static int anInt1478;
	public static int anInt2200 = 0;
	public static boolean aBoolean475;
	public static boolean fetchMusic = false;
	public static byte[] musicData;
	public int previousSong;
	public static int musicVolume = 64;//was: 255
	public int currentSong = -1;
	public int nextSong;
	public static int musicVolume2;
	public static int anInt139;
	public static boolean aBoolean995;
	public static int anInt116;
	
	public MusicPlayer(){
		constructMusic();
	}
	
	private static void loadMusicFile(int id){
		try {
		String file = Config.F_CACHE_DIR+"/music/"+id+".gz";
		if(!FileOperations.FileExists(file)){
			System.out.println("File: "+file+" does not exist!");
			return;
		}
		InputStream in = new GZIPInputStream(new FileInputStream(file));
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	    int nRead;
	    byte[] gzipInputBuffer = new byte['\ufde8'];
	    while ((nRead = in.read(gzipInputBuffer, 0, gzipInputBuffer.length)) != -1) {
	        buffer.write(gzipInputBuffer, 0, nRead);
	    }
	 
	    buffer.flush();
	    byte[] byteArray = buffer.toByteArray();
	    musicData = byteArray;
    	fetchMusic = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void playSong(int id){
		if(!mudclient.playMusic)
			return;
		if(id == -1)
			return;
		if(this.currentSong != id && musicVolume != 0 && this.previousSong == 0) {
            this.method58(18, musicVolume, false, id);
            this.currentSong = id;
         }
		/*if(id == -1 && this.previousSong == 0) {
            method55(false);
         } else if(id != -1 && this.currentSong != id && musicVolume != 0 && this.previousSong == 0) {
            this.method58(18, musicVolume, false, id);
         }*/
	}
	
	public void playQuickSong(int id, int time){
		if(!mudclient.playMusic)
			return;
		if(id == -1)
			return;
		if(musicVolume != 0 && time != -1) {
            this.method56(musicVolume, false, id);
            this.previousSong = time * 20;
         }
	}
	
	public void adjustVolume(int config) {
		short volume = 0;
		if (config == 0) {
			volume = 255;
		}

		if (config == 1) {
			volume = 192;
		}

		if (config == 2) {
			volume = 128;
		}

		if (config == 3) {
			volume = 64;
		}

		if (config == 4) {
			volume = 0;
		}

		if (volume != musicVolume) {
			if (musicVolume == 0 && this.currentSong != -1) {
				this.method56(volume, false, this.currentSong);
				this.previousSong = 0;
			} else if (volume != 0) {
				setVolume(volume);
			} else {
				method55(false);
				this.previousSong = 0;
			}

			musicVolume = volume;
		}
	}

	private static final boolean musicIsntNull() {
		return aClass56_749 != null;
	}

	private static final void setVolume(int i) {
		if (musicIsntNull()) {
			if (fetchMusic) {
				musicVolume2 = i;
			} else {
				method900(i);
			}
		}

	}
	
	private static final boolean constructMusic() {
		anInt720 = 20;

		try {
			//aClass56_749 = (Class56) Class.forName("Class56_Sub1_Sub1").newInstance();
			aClass56_749 = (Class56) new Class56_Sub1_Sub1();
			return true;
		} catch (Throwable var1) {
			return false;
		}
	}
	
	public static final void method49() {
		if (musicIsntNull()) {
			if (fetchMusic) {
				byte[] is = musicData;
				if (is != null) {
					if (anInt116 >= 0) {
						method684(aBoolean995, anInt116, musicVolume2, is);
					} else if (anInt139 >= 0) {
						method899(anInt139, -1, aBoolean995, is, musicVolume2);
					} else {
						method853(musicVolume2, is, aBoolean995);
					}

					fetchMusic = false;
				}
			}

			method368(0);
		}

	}
	
	private static final void method55(boolean bool) {
		if (musicIsntNull()) {
			method891(bool);
			fetchMusic = false;
		}
	}
	
	private final void method56(int i, boolean bool, int music) {
		if (musicIsntNull() && music != this.nextSong) {
			this.nextSong = music;
			loadMusicFile(this.nextSong);
			musicVolume2 = i;
			anInt139 = -1;
			aBoolean995 = true;
			anInt116 = -1;
		}
	}
	
	private final void method58(int i_30_, int volume, boolean bool, int music) {
		if (musicIsntNull()) {
			this.nextSong = music;
			loadMusicFile(this.nextSong);
			musicVolume2 = volume;
			anInt139 = -1;
			aBoolean995 = true;
			anInt116 = i_30_;
		}
	}
	
	public void method90() {//called in mainGameProcessor()

	      if(this.previousSong > 0) {
	         this.previousSong -= 20;
	         if(this.previousSong < 0) {
	            this.previousSong = 0;
	         }

	         if(this.previousSong == 0 && musicVolume != 0 && this.currentSong != -1) {
	            this.method56(musicVolume, false, this.currentSong);
	         }
	      }

	 }
	
	private static final void method368(int i) {
		if (aClass56_749 != null) {
			if (anInt478 < i) {
				if (anInt720 > 0) {
					--anInt720;
					if (anInt720 == 0) {
						if (aByteArray347 == null) {
							aClass56_749.method831(256);
						} else {
							aClass56_749.method831(anInt1478);
							anInt478 = anInt1478;
							aClass56_749.method827(anInt1478, aByteArray347, 0, aBoolean475);
							aByteArray347 = null;
						}

						anInt155 = 0;
					}
				}
			} else if (anInt720 > 0) {
				anInt155 += anInt2200;
				aClass56_749.method830(anInt478, anInt155);
				--anInt720;
				if (anInt720 == 0) {
					aClass56_749.method833();
					anInt720 = 20;
					anInt478 = -1;
				}
			}

			aClass56_749.method832(i - 122);
		}

	}
	
	private static final void method684(boolean bool, int i, int i_2_, byte[] is) {
		if (aClass56_749 != null) {
			if (anInt478 >= 0) {
				anInt2200 = i;
				if (anInt478 != 0) {
					int i_4_ = method1004(anInt478);
					i_4_ -= anInt155;
					anInt720 = (i_4_ + 3600) / i;
					if (anInt720 < 1) {
						anInt720 = 1;
					}
				} else {
					anInt720 = 1;
				}

				aByteArray347 = is;
				anInt1478 = i_2_;
				aBoolean475 = bool;
			} else if (anInt720 == 0) {
				method853(i_2_, is, bool);
			} else {
				anInt1478 = i_2_;
				aBoolean475 = bool;
				aByteArray347 = is;
			}
		}

	}

	public static final void method790() {
		if (aClass56_749 != null) {
			method891(false);
			if (anInt720 > 0) {
				aClass56_749.method831(256);
				anInt720 = 0;
			}

			aClass56_749.method828();
			aClass56_749 = null;
		}

	}
	
	private static final void method853(int i_2_, byte[] is, boolean bool) {
		if (aClass56_749 != null) {
			if (anInt478 >= 0) {
				aClass56_749.method833();
				anInt478 = -1;
				aByteArray347 = null;
				anInt720 = 20;
				anInt155 = 0;
			}

			if (is != null) {
				if (anInt720 > 0) {
					aClass56_749.method831(i_2_);
					anInt720 = 0;
				}

				anInt478 = i_2_;
				aClass56_749.method827(i_2_, is, 0, bool);
			}
		}

	}

	private static final void method891(boolean bool) {
		method853(0, (byte[]) null, bool);
	}
	
	private static final void method899(int i, int i_29_, boolean bool, byte[] is, int i_30_) {
		if (aClass56_749 != null) {
			if (i_29_ >= ~anInt478) {
				i -= 20;
				if (i < 1) {
					i = 1;
				}

				anInt720 = i;
				if (anInt478 == 0) {
					anInt2200 = 0;
				} else {
					int i_31_ = method1004(anInt478);
					i_31_ -= anInt155;
					anInt2200 = (anInt2200 - 1 + i_31_ + 3600) / anInt2200;
				}

				aBoolean475 = bool;
				aByteArray347 = is;
				anInt1478 = i_30_;
			} else if (anInt720 != 0) {
				aBoolean475 = bool;
				aByteArray347 = is;
				anInt1478 = i_30_;
			} else {
				method853(i_30_, is, bool);
			}
		}

	}
	
	private static final void method900(int i) {
		if (aClass56_749 != null) {
			if (anInt720 == 0) {
				if (anInt478 >= 0) {
					anInt478 = i;
					aClass56_749.method830(i, 0);
				}
			} else if (aByteArray347 != null) {
				anInt1478 = i;
			}
		}

	}

	private static final int method1004(int i) {
		return (int) (Math.log((double) i * 0.00390625D) * 868.5889638065036D + 0.5D);
	}

}
