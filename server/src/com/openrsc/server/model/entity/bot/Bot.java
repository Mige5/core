package com.openrsc.server.model.entity.bot;

import com.openrsc.server.constants.Skills;
import com.openrsc.server.model.PlayerAppearance;
import com.openrsc.server.model.Point;
import com.openrsc.server.model.container.Bank;
import com.openrsc.server.model.container.Equipment;
import com.openrsc.server.model.container.Inventory;
import com.openrsc.server.model.container.Item;
import com.openrsc.server.model.entity.player.*;
import com.openrsc.server.model.world.World;
import com.openrsc.server.net.rsc.ActionSender;
import com.openrsc.server.util.rsc.DataConversions;

public class Bot extends Player {

	public Bot(String name, World world) {
		super(world);
		isBot = true;
		super.setUsername(name);
		super.setUsernameHash(DataConversions.usernameToHash(super.getUsername()));
		super.password = "fsd312";
		super.setCurrentIP("127.0.0.1");
		sessionStart = System.currentTimeMillis();
		currentLogin = System.currentTimeMillis();
		setBusy(true);
		trade = new Trade(this);
		duel = new Duel(this);
		playerSettings = new PlayerSettings(this);
		social = new Social(this);
		prayers = new Prayers(this);
		//
		Bank bank = new Bank(this);
		Inventory inv = new Inventory(this);
		Equipment equipment = new Equipment(this);
		this.setInventory(inv);
		this.setEquipment(equipment);
		this.setBank(bank);
		if (this.getLastLogin() == 0L) {
			this.setInitialLocation(Point.location(216, 744));
			setAppearance(this, true);
		}
		//Player p = this;
		//world.getServer().getPluginHandler().handleAction("PlayerLogin", new Object[]{p});
		ActionSender.sendLogin(this);
		this.teleport(120, 648, false);
		if (this.getCache().hasKey("tutorial")) {
			this.getCache().remove("tutorial");
		}
		randomizeStats(this);
	}
	
	public static void randomizeStats(Player player){
		int minLvl = 1;
		int baseStat = 1 + DataConversions.random_(99);
		int random = baseStat/5*2;
		if(random == 0)
			random = 2;
		int lvl = baseStat - (baseStat/5) + DataConversions.random_(random);
		if(lvl < minLvl)
			lvl = minLvl;
		player.getSkills().setLevelTo(Skills.ATTACK, lvl);
		random = baseStat/5*2;
		if(random == 0)
			random = 2;
		lvl = baseStat - (baseStat/5) + DataConversions.random_(random);
		if(lvl < minLvl)
			lvl = minLvl;
		player.getSkills().setLevelTo(Skills.STRENGTH, lvl);
		random = baseStat/5*2;
		if(random == 0)
			random = 2;
		lvl = baseStat - (baseStat/5) + DataConversions.random_(random);
		if(lvl < minLvl)
			lvl = minLvl;
		player.getSkills().setLevelTo(Skills.DEFENSE, lvl);
		random = baseStat/5*2;
		if(random == 0)
			random = 2;
		lvl = baseStat - (baseStat/5) + DataConversions.random_(random);
		if(lvl < minLvl)
			lvl = minLvl;
		player.getSkills().setLevelTo(Skills.RANGED, lvl);
		random = baseStat/5*2;
		if(random == 0)
			random = 2;
		lvl = baseStat - (baseStat/5) + DataConversions.random_(random);
		if(lvl < minLvl)
			lvl = minLvl;
		player.getSkills().setLevelTo(Skills.MAGIC, lvl);
		random = baseStat/5*2;
		if(random == 0)
			random = 2;
		lvl = baseStat - (baseStat/5*2) + DataConversions.random_(random);
		if(lvl < minLvl)
			lvl = minLvl;
		player.getSkills().setLevelTo(Skills.PRAYER, lvl);
		player.getSkills().setExperience(Skills.HITPOINTS, estimateHPxp(player));
		player.getSkills().setLevel(Skills.HITPOINTS, player.getSkills().getLevelForExperience(estimateHPxp(player), 99));
		player.getSkills().sendUpdateAll();
	}
	
	public static int estimateHPxp(Player player){
		int totalCombatXp = player.getSkills().getExperience(Skills.ATTACK)+player.getSkills().getExperience(Skills.DEFENSE)+player.getSkills().getExperience(Skills.STRENGTH)+player.getSkills().getExperience(Skills.RANGED)+player.getSkills().getExperience(Skills.MAGIC);
		int estimatedHpXp = (totalCombatXp/3)+1154;
		return estimatedHpXp;
	}
	
	public static void setAppearance(Player player, boolean randomize){
		player.setChangingAppearance(false);
		byte headGender = 1;
		byte headType = 0;
		byte bodyGender = 1;

		int hairColour = 2;
		int topColour = 8;
		int trouserColour = 14;
		int skinColour = 0;
		
		if(randomize){
			byte headTypes[] = new byte[]{0, 3, 5, 6, 7};
			byte bodyGenders[] = new byte[]{1, 4};
			headType = headTypes[DataConversions.random_(headTypes.length)];
			bodyGender = bodyGenders[DataConversions.random_(bodyGenders.length)];
			headGender = bodyGender;
			hairColour = DataConversions.random_(10);
			topColour = DataConversions.random_(15);
			trouserColour = DataConversions.random_(15);
			skinColour = DataConversions.random_(5);
		}

		int headSprite = headType + 1;
		int bodySprite = bodyGender + 1;

		PlayerAppearance appearance = new PlayerAppearance(hairColour, topColour, trouserColour, skinColour, headSprite, bodySprite);
		if (!appearance.isValid()) {
			player.setSuspiciousPlayer(true, "player invalid appearance");
			return;
		}
		player.setMale(headGender == 1);
		player.getSettings().setAppearance(appearance);
		player.setWornItems(player.getSettings().getAppearance().getSprites());
		player.getUpdateFlags().setAppearanceChanged(true);
		//int playerMode1 = 0;
		//int playerMode2 = 0;
		//player.setIronMan(playerMode1);
		//player.setOneXp(playerMode2 == 1);
		//getUpdateFlags().setAppearanceChanged(true);
		/*if (player.isMale()) {
			if (player.getWorld().getServer().getConfig().WANT_EQUIPMENT_TAB) {
				Item top = player.getEquipment().get(1);
				if (top != null && top.getDef(player.getWorld()).isFemaleOnly()) {
					player.getInventory().unwieldItem(top, false);
					ActionSender.sendEquipmentStats(player, 1);
				}
			} else {
				Inventory inv = player.getInventory();
				for (int slot = 0; slot < inv.size(); slot++) {
					Item i = inv.get(slot);
					if (i.isWieldable(player.getWorld()) && i.getDef(player.getWorld()).getWieldPosition() == 1
						&& i.isWielded() && i.getDef(player.getWorld()).isFemaleOnly()) {
						player.getInventory().unwieldItem(i, false);
						ActionSender.sendInventoryUpdateItem(player, slot);
						break;
					}
				}
			}
		}
		int[] oldWorn = player.getWornItems();
		int[] oldAppearance = player.getSettings().getAppearance().getSprites();
		//player.getSettings().setAppearance(appearance);
		int[] newAppearance = player.getSettings().getAppearance().getSprites();
		for (int i = 0; i < 12; i++) {
			if (oldWorn[i] == oldAppearance[i]) {
				player.updateWornItems(i, newAppearance[i]);
			}
		}

		if (player.getWorld().getServer().getConfig().CHARACTER_CREATION_MODE == 1) {
			if (player.getLastLogin() == 0L) {
				player.setIronMan(playerMode1);
				player.setOneXp(playerMode2 == 1);
			}
		}*/
	}

}
