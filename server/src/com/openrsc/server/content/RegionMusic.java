package com.openrsc.server.content;

import com.openrsc.server.model.entity.player.Player;
import com.openrsc.server.net.rsc.ActionSender;

public class RegionMusic {
	
	int regionId;
	int songId;
	
	//14186, tav. dung 1
	//15469, ladders down west of ardy monastery
	
	//TODO: fill data for rest of the regions
	private static RegionMusic regionSongs[] = {

		new RegionMusic(12335, 20),//digsite
		new RegionMusic(12336, 75),//exam centre
		new RegionMusic(12583, 329),//wild
		new RegionMusic(12584, 331),//wild
		new RegionMusic(12585, 182),//wild
		new RegionMusic(12586, 14),//wild
		new RegionMusic(12587, 326),//wild
		new RegionMusic(12588, 337),//wild
		new RegionMusic(12589, 121),//wild
		new RegionMusic(12590, 93),//ne of varrock
		new RegionMusic(12591, 157),//east of varrock
		new RegionMusic(12592, 111),//varrock e mine
		new RegionMusic(12593, 123),//south of varrock e mine
		new RegionMusic(12594, 36),//east of lumb
		new RegionMusic(12595, 50),//alkharid bank
		new RegionMusic(12596, 69),//shantay pass
		new RegionMusic(12597, 174),//south of shantay pass
		new RegionMusic(12610, 93),//ne of varrock +1
		new RegionMusic(12614, 50),//alkharid bank +1
		new RegionMusic(12839, 127),//wild
		new RegionMusic(12840, 332),//wild
		new RegionMusic(12841, 189),//wild
		new RegionMusic(12842, 120),//wild
		new RegionMusic(12843, 332),//wild
		new RegionMusic(12844, 337),//wild
		new RegionMusic(12845, 169),//wild
		new RegionMusic(12846, 177),//varrock castle
		new RegionMusic(12847, 125),//varrock center
		new RegionMusic(12848, 106),//south of varrock
		new RegionMusic(12849, 2),//lumbridge sheeps
		new RegionMusic(12850, 76),//lumbridge castle
		new RegionMusic(12851, 145),//south of lumb
		new RegionMusic(12852, 124),//west of shantay pass
		new RegionMusic(12853, 79),//sw of shantay pass
		new RegionMusic(12866, 177),//varrock castle +1
		new RegionMusic(12867, 125),//varrock center +1
		new RegionMusic(12870, 76),//lumbridge castle +1
		new RegionMusic(12890, 76),//lumbridge castle +2
		new RegionMusic(12905, 144),//varrock sewer
		new RegionMusic(12907, 106),//varrock, phoenix gang basement
		new RegionMusic(12909, 76),//lumbridge castle basement
		new RegionMusic(13095, 106),//wild
		new RegionMusic(13096, 332),//wild
		new RegionMusic(13097, 189),//wild
		new RegionMusic(13098, 67),//wild
		new RegionMusic(13099, 476),//wild
		new RegionMusic(13100, 10),//wild
		new RegionMusic(13101, 113),//wild
		new RegionMusic(13102, 56),//west of varrock castle
		new RegionMusic(13103, 175),//varrock w bank
		new RegionMusic(13104, 116),//champions guild
		new RegionMusic(13105, 163),//nw of lumb
		new RegionMusic(13106, 327),//west of lumb
		new RegionMusic(13107, 64),//sw of lumb
		new RegionMusic(13109, 79),//bedabin camp
		new RegionMusic(13124, 116),//champions guild +1
		new RegionMusic(13125, 163),//nw of lumb +1
		new RegionMusic(13144, 163),//nw of lumb +2
		new RegionMusic(13161, 144),//edge dungeon 2
		new RegionMusic(13162, 330),//varrock w bank basement
		new RegionMusic(13351, 13),//wild
		new RegionMusic(13352, 475),//wild
		new RegionMusic(13353, 42),//wild
		new RegionMusic(13354, 435),//wild
		new RegionMusic(13355, 176),//wild
		new RegionMusic(13356, 8),//wild
		new RegionMusic(13357, 182),//wild
		new RegionMusic(13358, 98),//edge
		new RegionMusic(13359, 141),//barbarian village
		new RegionMusic(13360, 333),//draynor manor
		new RegionMusic(13361, 151),//north of draynor
		new RegionMusic(13362, 3),//draynor bank
		new RegionMusic(13363, 85),//wizard tower
		new RegionMusic(13364, 62),//tutorial island
		new RegionMusic(13377, 98),//edge +1
		new RegionMusic(13380, 333),//draynor manor +1
		new RegionMusic(13381, 151),//north of draynor +1
		new RegionMusic(13383, 85),//wizard tower +1
		new RegionMusic(13399, 333),//draynor manor +2
		new RegionMusic(13402, 85),//wizard tower +2
		new RegionMusic(13417, 98),//edge dungeon
		new RegionMusic(13418, 141),//varrock hill giants
		new RegionMusic(13419, 338),//draynor manor basement
		new RegionMusic(13422, 85),//wizard tower basement
		new RegionMusic(13607, 334),//wild
		new RegionMusic(13608, 475),//wild
		new RegionMusic(13609, 42),//wild
		new RegionMusic(13610, 159),//wild
		new RegionMusic(13611, 66),//wild
		new RegionMusic(13612, 160),//wild
		new RegionMusic(13613, 96),//wild
		new RegionMusic(13614, 102),//monastery
		new RegionMusic(13615, 150),//south of monastery
		new RegionMusic(13616, 15),//west of draynor manor
		new RegionMusic(13617, 49),//north of port sarim
		new RegionMusic(13618, 35),//port sarim
		new RegionMusic(13619, 105),//south of port sarim
		new RegionMusic(13633, 102),//black knight fortress +1
		new RegionMusic(13635, 15),//west of draynor manor +1	
		new RegionMusic(13637, 35),//port sarim +1
		new RegionMusic(13638, 35),//port sarim +1
		new RegionMusic(13653, 102),//black knight fortress +2
		new RegionMusic(13674, 325),//dwarven mine
		new RegionMusic(13675, 325),//dwarven mine 2
		new RegionMusic(13677, 35),//port sarim, dragon slayer ship
		new RegionMusic(13678, 108),//blurite cave
		new RegionMusic(13863, 52),//wild
		new RegionMusic(13864, 37),//wild
		new RegionMusic(13865, 37),//wild
		new RegionMusic(13866, 43),//wild
		new RegionMusic(13867, 183),//wild
		new RegionMusic(13868, 435),//wild
		new RegionMusic(13869, 34),//wild
		new RegionMusic(13870, 23),//goblin village
		new RegionMusic(13871, 54),//north of falador
		new RegionMusic(13872, 72),//falador center
		new RegionMusic(13873, 127),//north of rimmington
		new RegionMusic(13874, 12),//rimmington
		new RegionMusic(13875, 180),//se of port sarim
		new RegionMusic(13891, 72),//falador center, +1
		new RegionMusic(13892, 127),//north of rimmington +1
		new RegionMusic(13894, 12),//rimmington +1
		new RegionMusic(13912, 72),//falador castle, +2
		new RegionMusic(14126, 77),//north of taverley
		new RegionMusic(14127, 18),//taverley
		new RegionMusic(14128, 186),//west of falador
		new RegionMusic(14129, 107),//crafting guild
		new RegionMusic(14130, 138),//west of rimmington
		new RegionMusic(14131, 92),//karamja
		new RegionMusic(14132, 479),//south of karamja
		new RegionMusic(14133, 89),//ne of shilo village
		new RegionMusic(14134, 94),//east of shilo village
		new RegionMusic(14135, 78),//se of shilo village
		new RegionMusic(14146, 18),//taverley +1
		new RegionMusic(14149, 138),//melzar maze +1
		new RegionMusic(14169, 138),//melzar maze +2
		new RegionMusic(14382, 87),//white wolf mountain
		new RegionMusic(14383, 119),//catherby
		new RegionMusic(14384, 324),//entrana
		new RegionMusic(14385, 170),//crandor
		new RegionMusic(14386, 170),//crandor
		new RegionMusic(14387, 172),//karamja volcano
		new RegionMusic(14388, 166),//south of karamja volcano, rs2 music: 162 (rs2 music for ship yard: 166)
		new RegionMusic(14389, 165),//east of tai bwo wannai
		new RegionMusic(14390, 90),//shilo village
		new RegionMusic(14391, 172),//south of shilo village
		new RegionMusic(14445, 25),//karamja volcano (inside)
		new RegionMusic(14638, 104),//nw of catherby
		new RegionMusic(14639, 74),//west of catherby
		new RegionMusic(14640, 324),//west of entrana
		new RegionMusic(14642, 6),//north of brimhaven
		new RegionMusic(14643, 55),//brimhaven
		new RegionMusic(14644, 58),//south of brimhaven
		new RegionMusic(14645, 117),//tai bwo wannai
		new RegionMusic(14646, 114),//south of tai bwo wannai
		new RegionMusic(14647, 129),//kharazi jungle west
		new RegionMusic(14893, 21),//north of camelot
		new RegionMusic(14894, 7),//camelot
		new RegionMusic(14895, 184),//south of catherby
		new RegionMusic(14896, 192),//legends guild
		new RegionMusic(14897, 192),//south of legends guild
		new RegionMusic(14898, 115),//se of east ardougne
		new RegionMusic(14899, 164),//west of brimhaven
		new RegionMusic(15150, 140),//mcgrubor wood
		new RegionMusic(15151, 60),//south of mcgrubor wood
		new RegionMusic(15152, 81),//west of legends guild
		new RegionMusic(15153, 99),//east ardougne
		new RegionMusic(15154, 70),//south of east ardougne
		new RegionMusic(15155, 167),//port khazard
		new RegionMusic(15156, 161),//south of port khazard
		new RegionMusic(15406, 109),//coal trucks
		new RegionMusic(15407, 193),//fishing guild
		new RegionMusic(15408, 133),//nw of east ardougne
		new RegionMusic(15409, 191),//west of east ardougne
		new RegionMusic(15410, 152),//sw of east ardougne
		new RegionMusic(15411, 27),//fight arena
		new RegionMusic(15412, 185),//yanille
		new RegionMusic(15413, 188),//south of yanille
		new RegionMusic(15414, 128),//feldip hills east
		new RegionMusic(15662, 32),//west of coal trucks
		new RegionMusic(15663, 82),//west of fishing guild
		new RegionMusic(15664, 328),//north of west ardougne
		new RegionMusic(15665, 5),//west ardougne
		new RegionMusic(15666, 24),//south of west ardougne
		new RegionMusic(15667, 148),//tree gnome village
		new RegionMusic(15668, 83),//south of tree gnome village
		new RegionMusic(15669, 159),//sw of yanille
		new RegionMusic(15670, 80),//feldip hills west
		new RegionMusic(15909, 57),//rune ess
		new RegionMusic(15918, 22),//north of gnome agility
		new RegionMusic(15919, 33),//gnome agility
		new RegionMusic(15920, 155),//south of gnome agility
		new RegionMusic(15921, 10),//west of west ardougne
		new RegionMusic(15922, 41),//sw of west ardougne
		new RegionMusic(15923, 110),//observatory
		new RegionMusic(15939, 33),//gnome agility +1
		new RegionMusic(15958, 33),//gnome agility +2
		new RegionMusic(16174, 112),//nw of gnome agility
		new RegionMusic(16175, 101),//west of gnome agility
		new RegionMusic(16176, 130),//sw of gnome agility
		new RegionMusic(16177, 10),//north of nw of observatory
		new RegionMusic(16178, 10),//nw of observatory
		new RegionMusic(17189, 65),//mind altar
		new RegionMusic(17445, 52),//air altar
	};
	
	private RegionMusic(int regionId_, int songId_){
		this.regionId = regionId_;
		this.songId = songId_;
	}
	
	public static void playRegionSong(Player player){
		int regionId = player.getRegionId();
		if(player.prevRegionId != regionId){
			player.prevRegionId = regionId;
			int songId = getSongForRegion(regionId);
			if(songId != -1){
				ActionSender.sendSong(player, songId);
			} else {
				player.message("region id: "+regionId+" does not have music defined.");
				ActionSender.sendSong(player, 76);
			}
		}
	}
	
	private int getRegionId(){
		return this.regionId;
	}
	
	private int getSongId(){
		return this.songId;
	}

	public static int getSongForRegion(int regionId){
		for(RegionMusic rm : regionSongs){
			if(rm.getRegionId() != regionId)
				continue;
			return rm.getSongId();
		}
		return -1;
	}
	
}
